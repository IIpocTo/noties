/**
 * Data Access Objects used by WebSocket services.
 */
package com.noties.web.websocket.dto;
