/**
 * View Models used by Spring MVC REST controllers.
 */
package com.noties.web.rest.vm;
